package com.derinaldis.testjdbc.logging;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.derinaldis.testjdbc.PropertyReader;

public class MyLogger {
	private static Level defaultLevel = Level.FINEST;

	private static MyLogger instance;

	public static MyLogger getInstance() {
		if (instance == null) {
			instance = new MyLogger();
		}
		return instance;
	}

	private Logger logger;

	private MyLogger() {
		logger = Logger.getLogger("");
		Level level = defaultLevel;
		try {
			level = Level.parse(PropertyReader.getInstance().get("logLevel"));
		} catch (Exception e) {
			warning("error parsing log level : '" + PropertyReader.getInstance().get("logLevel")
					+ "'. Setting log level to " + defaultLevel);

		}
		logger.setLevel(level);
		for (Handler h : logger.getHandlers()) {
			h.setLevel(level);
		}
		info("log level set to " + logger.getLevel());
	}

	public void info(String s) {
		logger.info(s);
	}

	public void severe(String s) {
		logger.severe(s);
	}

	public void warning(String s) {
		logger.warning(s);
	}

	public void fine(String s) {
		logger.fine(s);
	}
}
