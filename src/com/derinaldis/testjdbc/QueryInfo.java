package com.derinaldis.testjdbc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QueryInfo {
	static public enum Action {
		SELECT, INSERT, UNKNOWN
	}

	final static String FERRE = "0";
	final static String FANLM = "N";

	final static int VALUE_LENGTH = 2;

	String VALUE = "";
	String REF_DATE = "";

	Action action = Action.UNKNOWN;

	protected boolean filledIn;

	List<String> errors;

	public QueryInfo() {
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}


	public String getVALUE() {
		return VALUE;
	}

	public void setVALUE(String VALUE) {
		this.VALUE = VALUE;
	}

	public String getREF_DATE() {
		return REF_DATE;
	}

	public void setREF_DATE(String REF_DATE) {
		this.REF_DATE = REF_DATE;
	}

	public boolean isFilledIn() {
		return filledIn;
	}

	public boolean hasErrors() {
		return !(errors == null || errors.size() == 0);
	}

	public List<String> getErrors() {
		return errors;
	}

	public void checkParams() {
		errors = new ArrayList<String>();
		checkAction();
		checkVALUE();
		checkREF_DATE();
		filledIn = true;
	}

	void checkAction() {
		if (action == Action.UNKNOWN) {
			errors.add("Azione non valida");
		}
	}

	void checkVALUE() {
		if (VALUE != null && VALUE.trim().length() > 0 && VALUE.trim().length() <= VALUE_LENGTH) {
			return;
		}
		errors.add("Tipo tabulato non valido");
	}

	void checkREF_DATE() {
		if (parseREF_DATE() != null) {
			return;
		}
		errors.add("Data non valida");
	}

	public String getREF_DATE_SQL() {
		Date date = parseREF_DATE();
		if (date == null) {
			return "";
		}
		return new SimpleDateFormat("yyyyMMdd").format(date);
	}

	private Date parseREF_DATE() {
		SimpleDateFormat formFormat = new SimpleDateFormat("dd-MM-yyyy");
		formFormat.setLenient(false);
		if (REF_DATE != null) {
			try {
				return formFormat.parse(REF_DATE);
			} catch (Exception e) {
			}
		}
		return null;
	}
}
