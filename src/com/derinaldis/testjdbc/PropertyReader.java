package com.derinaldis.testjdbc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {
	private static String properties = "application.properties";

	private static PropertyReader instance;
	
	public static PropertyReader getInstance() {
		if (instance == null) {
			try {
				instance = new PropertyReader();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		return instance;
	}
	
	Properties props;

	private PropertyReader() throws IOException {
	    InputStream fis = getClass().getResourceAsStream("/" + Defaults.resourceDir + "/" + properties);
	    props = new Properties();
	    props.load(fis);
	}

	public String get(String prop) {
	    return props.getProperty("logLevel");
	}
}
