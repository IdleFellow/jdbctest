package com.derinaldis.testjdbc;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.derinaldis.testjdbc.logging.MyLogger;

@WebServlet("/FormController")
public class FormController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/plain");

		MyLogger.getInstance().info("action_select pushed: " + req.getParameter("action_select"));
		MyLogger.getInstance().info("action_insert pushed: " + req.getParameter("action_insert"));

		QueryInfo info = new QueryInfo();
		if (req.getParameter("action_select") != null) {
			info.setAction(QueryInfo.Action.SELECT);
		} else if (req.getParameter("action_insert") != null) {
			info.setAction(QueryInfo.Action.INSERT);
		}
		info.setVALUE(req.getParameter("VALUE"));
		info.setREF_DATE(req.getParameter("REF_DATE"));

		info.checkParams();

		HttpSession ses = req.getSession(true);
		ses.setAttribute("info", info);
		ses.setAttribute("errors", info.getErrors());

		RequestDispatcher rd = req.getRequestDispatcher("index.jsp");
		rd.forward(req, res);
	}
}
