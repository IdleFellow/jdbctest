<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="com.derinaldis.testjdbc.QueryInfo"%>
<!DOCTYPE html>
<jsp:useBean id="info" class="com.derinaldis.testjdbc.QueryInfo"
	scope="session" />
<html>
<head>
<%@ include file="common/head.html"%>
</head>
<body>
	<%@ include file="common/body.html"%>
	<div id="form-container">
		<form action="FormController" method="post" class="form-style-9">
			<%
				if (info.hasErrors()) {
					out.print("<section class=\"errors\">");
					out.print("<ul>");
					for (String error : info.getErrors()) {
						out.print("<li>" + error + "</li>");
					}
					out.print("</ul>");
					out.print("</section>");
				}
			%>
			<ul>
				<li>
					<div class="align-left">
						<label for="REF_DATE"> Data di riferimento&nbsp; </label> <input
							type="text" name="REF_DATE" placeholder="Data di riferimento"
							value="<%out.print(info.getREF_DATE());%>" id="REF_DATE"
							class="field-style field-full" />
					</div>
				</li>
				<li>
					<div class="align-left">
						<label for="VALUE"> Valore&nbsp; </label> <input type="text"
							name="VALUE" placeholder="Valore"
							value="<%out.print(info.getVALUE());%>" id="VALUE"
							class="field-style field-full" />
					</div>
				</li>
				<li><input type="submit" name="action_select"
					value="Conferma inserimento" /></li>
			</ul>
		</form>
		<div class="form-style-9">
			<ul>
				<li>
					<div class="align-left">
						<label for="query">Query&nbsp;</label>
						<textarea name="query" rows="7" , cols="46" , readonly id="query"
							class="field-style"></textarea>
					</div>
				</li>
				<li><span>&nbsp;</span></li>
			</ul>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		$(function() {
			$.datepicker.setDefaults($.datepicker.regional["it"]);
			$("#REF_DATE").datepicker({
				dateFormat : "dd-mm-yy",
				changeMonth : true,
				changeYear : true,
				showButtonPanel : true,
				maxDate : "+0m"
			});
		});
	</script>
</body>
</html>